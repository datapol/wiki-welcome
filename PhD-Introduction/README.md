This [set of slides](PhD-Edmond-Boyer-2021.pdf) was presented by Edmond Boyer to new Inria PhD
students in October 2021. All the credit is his. Please do not
redistribute.

Another related set of slides is the [Introduction to Reproducible
Research and Open Science](https://github.com/alegrand/SMPE/raw/master/lectures/talk_21_10_13_Grenoble.pdf)
given on the same day by Arnaud Legrand